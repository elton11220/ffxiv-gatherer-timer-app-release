# FFXIV 采集时钟 App
使用 `React Native & TypeScript` 开发的 `FFXIV 采集时钟 App`

设备需求：`Android 9.0(SDK28)` 及以上版本系统

## 下载帮助
App 分别为以下架构生成不同安装包：
- `arm64-v8a`
- `armeabi-v7a`

大多数情况下，你可以选择下载 `app-arm64-v8a-release.apk`

如果你不知道手机的处理器架构，请下载 `app-universal-release.apk` (体积较大)

## 问题反馈
有以下几个提交反馈/建议的方式：
1. 直接在仓库里提交 `issue`
2. 发送邮件到 `793705875@qq.com`
3. NGA论坛内帖子评论 [https://bbs.nga.cn/read.php?tid=36031067](https://bbs.nga.cn/read.php?tid=36031067)
4. 加入QQ频道 [FF14 幻想科技](https://pd.qq.com/s/85jbt6zrf)
